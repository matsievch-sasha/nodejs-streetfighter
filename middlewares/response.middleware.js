const responseMiddleware = (req, res, next) => {
    // TODO: Implement middleware that returns result of the query

    if (res.data) {
        res.status(200).json(res.data);
    } else if(res.err.error === true){
      res.status(400).send(res.err);
    } else {
      res.status(404).send(res.err);
    }
     next();
}

exports.responseMiddleware = responseMiddleware;