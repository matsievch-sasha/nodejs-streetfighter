const { fighter } = require('../models/fighter');
const FighterService = require('../services/fighterService')

const createFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during creation
  const { error, message } = checkCreateFighter(req.body, fighter)
  if (!error) {
    addFields(fighter, req.body);
    res.data = req.body;
  } else {
    res.err = { error, message };
  }
  next();
}

const updateFighterValid = (req, res, next) => {
  let error = false;
  let message = '';
  const fighterBase = FighterService.search(req.params);
  if (fighterBase) {
    ({ error, message } = checkFighterUpdate(req.body, fighter))
  } else {
    error = true;
    message = 'Fighter not found!';
  }
  if (!error) {
    res.data = req.body;
  } else {
    res.err = { error, message };
  }
  next();
}

function checkCreateFighter(obj, fighter) {
  let error = false;
  let message = '';
  const checkFunctions = [isEmptyObject, checkFields, objHasFields, checkAllValue, checkMatchName];
  for (let i = 1, n = checkFunctions.length; i < n; i++) {
    ({ error, message } = checkFunctions[i](obj, fighter));
    if (error) return { error, message }
  }

  return { error, message };
}

function checkFighterUpdate(obj, fighter) {
  let error = false;
  let message = '';
  const checkFunctions = [isEmptyObject, checkFields, checkAllValue];
  for (let i = 1, n = checkFunctions.length; i < n; i++) {
    ({ error, message } = checkFunctions[i](obj, fighter));
    if (error) return { error, message }
  }
  return { error, message };
}

function checkAllValue(obj) {
  let error = false;
  let message = '';
  for (let name in obj) {
    ({ error, message } = checkValue(name, obj[name]));
    if (error) break;
  }
  return { error, message }
}

function objHasFields(obj, fighter) {
  let error = false;
  let message = '';
  for (let name in fighter) {
    if (name === 'id' || name === 'health') continue;
    if (obj[name] === undefined || obj[name] === null || obj[name] === "") {
      message = 'all fields must be filled';
      error = true;
    }
  }
  return { error, message };
}

function checkValue(name, value) {
  let error = false;
  let message = '';
  switch (name) {
    case 'name':
      let regexpName = /^[a-zа']+$/i;
      if (!regexpName.test(value)) {
        message = 'the name must contain only letters of the Latin alphabet'
        error = true;
      }
      break;
    case 'power':
      if (checkNumbers(value)) {
        if (+value <= 0 || +value >= 100) {
          message = 'the power must be between 1 and 99'
          error = true;
        }
      } else {
        message = '"power" must number'
        error = true;
      }
      break;
    case 'defense':
      if (checkNumbers(value)) {
        if (+value < 1 || +value > 10) {
          message = 'the defense must be between 1 and 10'
          error = true;
        }
      } else {
        message = '"defense" must number'
        error = true;
      }
      break;
  }
  return { error, message }
}

function checkNumbers(value) {
  if (isFinite(value)) {
    if (value === null || value === '' || typeof (value) === "string") return false;
    return true;
  }
  return false;
}

function checkFields(obj, examplObj) {
  let error = false;
  let message = '';
  let examplObjFields = Object.keys(examplObj);
  for (const name in obj) {
    if (name === 'id' || name === 'health') {
      error = true;
      message = 'the request must not contain id or health';
      break;
    }
    if (!examplObjFields.includes(name)) {
      error = true;
      message = 'the request contains an unsupported field';
    }
  }
  return { error, message };
}

function isEmptyObject(obj) {
  let error = false;
  let message = '';
  if (Object.keys(obj).length === 0) {
    message = 'all fields must be filled';
    error = true;
  }
  return { error, message };
}

function checkMatchName(obj) {
  let error = false;
  let message = '';
  if (FighterService.search({ "name": obj.name })) {
    message = 'the fighter with that name already exists';
    error = true;
  }
  return { error, message };
}

function addFields(objOne, objTwo) {
  for (let key in objOne) {
    if (objTwo[key] === undefined) {
      objTwo[key] = objOne[key];
    }
  }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;