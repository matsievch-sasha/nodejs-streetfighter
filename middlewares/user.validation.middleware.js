const { user } = require('../models/user');
const UserService = require('../services/userService');

const createUserValid = (req, res, next) => {
  const { error, message } = checkCreateUser(req.body, user);
  if (!error) {
    res.data = req.body;
  } else {
    res.err = { error, message };
  }
  next();
}
const updateUserValid = (req, res, next) => {
  let error = false;
  let message = '';
  const userBase = UserService.search(req.params);
  if (userBase) {
    ({ error, message } = checkUpdateUser(req.body, user));
  } else {
    error = true;
    message = 'User not found!';
  }
  if (!error) {
    res.data = req.body;
  } else {
    res.err = { error, message };
  }
  next();
}
function checkCreateUser(obj, user) {
  let error = false;
  let message = '';
  const checkFunctions = [isEmptyObject, checkFields, objHasFields, checkAllValue];
  for (let i = 1, n = checkFunctions.length; i < n; i++) {
    ({ error, message } = checkFunctions[i](obj, user));
    if (error) return { error, message }
  }
  return { error, message };
}

function checkUpdateUser(obj, user) {
  let error = false;
  let message = '';
  const checkFunctions = [isEmptyObject, checkFields, checkAllValue];
  for (let i = 1, n = checkFunctions.length; i < n; i++) {
    ({ error, message } = checkFunctions[i](obj, user));
    if (error) return { error, message }
  }
  return { error, message };
}

function checkAllValue(obj) {
  let error = false;
  let message = '';
  for (let name in obj) {
    ({ error, message } = checkValue(name, obj[name]));
    if (error) break;
  }
  return { error, message }
}

function objHasFields(obj, user) {
  let error = false;
  let message = '';
  for (let name in user) {
    if (name === 'id') continue;
    if (obj[name] === undefined || obj[name].trim() === "") {
      message = 'all fields must be filled';
      error = true;
    }
  }
  return { error, message };
}

function checkValue(name, value) {
  let error = false;
  let message = '';
  switch (name) {
    case 'email':
      let regexpEmail = /^[a-z0-9]+@gmail\.com$/;
      if (!regexpEmail.test(value)) {
        message = 'Please, use a valid email address (only Gmail.com email address)';
        error = true;
      }
      break;
    case 'password':
      if (value.length < 3) {
        message = 'Password must be at least 3 characters'
        error = true;
      }
      break;
    case 'firstName':
    case 'lastName':
      let regexpName = /^[a-zа-яёіїє']+$/i;
      if (!regexpName.test(value)) {
        message = 'the name must contain only letters of the Latin or Cyrillic alphabet'
        error = true;
      }
      break;
    case 'phoneNumber':
      let regexpPhone = /^\+380\d{9}$/
      if (!regexpPhone.test(value)) {
        message = 'phone number must be in the format +380xxxxxxxxx'
        error = true;
      }
      break;
  }
  return { error, message }
}

function checkFields(obj, examplObj) {
  let error = false;
  let message = '';
  let examplObjFields = Object.keys(examplObj);
  for (const name in obj) {
    if (name === 'id') {
      error = true;
      message = 'the request must not contain id';
      break;
    }
    if (!examplObjFields.includes(name)) {
      error = true;
      message = 'the request contains an unsupported field';
    }
  }
  return { error, message };
}

function isEmptyObject(obj) {
  let error = false;
  let message = '';
  if (Object.keys(obj).length == 0) {
    message = 'all fields must be filled';
    error = true;
  }
  return { error, message };
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
