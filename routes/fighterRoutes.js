const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter

router.get('/:id', function (req, res, next) {
  const fighter = FighterService.search(req.params);
  if (fighter) {
    res.data = fighter;
  } else {
    res.err = "Fighter not found";
  }
  next();
}, responseMiddleware);

router.get('/', function (req, res, next) {
  const fighters = FighterService.getAll();
  if (fighters) {
    res.data = fighters;
  } else {
    res.err = 'Fighters not found!';
  }
  next();
}, responseMiddleware);

router.post('/', createFighterValid, function (req, res, next) {
  try {
    if (res.data) {
      const fighter = FighterService.create(res.data);
      res.data = fighter;
    }
  } catch (err) {
    res.err = err.message;
  } finally {
    next();
  }
}, responseMiddleware);

router.put('/:id', updateFighterValid, function (req, res, next) {
  try {
    if (res.data) {
      const fighter = FighterService.update(req.params.id, res.data);
      res.data = fighter;
    }
  } catch (err) {
    res.err = err.message;
  } finally {
    next();
  }
}, responseMiddleware);

router.delete('/:id', function (req, res, next) {
  try {
    const fighter = FighterService.delete(req.params.id)
    res.data = fighter;
  } catch (err) {
    res.err = err.message;
  } finally {
    next();
  }
}, responseMiddleware);

module.exports = router;