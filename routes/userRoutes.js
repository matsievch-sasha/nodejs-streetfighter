const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user

router.get('/:id', function (req, res, next) {
  const user = UserService.search(req.params);
  if (user) {
    res.data = user;
  } else {
    res.err = "User not found!";
  }
  next();
}, responseMiddleware);

router.get('/', function (req, res, next) {
  const users = UserService.getAll();
  if (users) {
    res.data = users;
  } else {
    res.err = "Users not found!";
  }
  next();
}, responseMiddleware);

router.post('/', createUserValid, function (req, res, next) {
  try {
    if (res.data) {
      const user = UserService.create(res.data);
      res.data = user;
    }
  } catch (err) {
    res.err = err.message;
  } finally {
    next();
  }
}, responseMiddleware);

router.put('/:id', updateUserValid, function (req, res, next) {
  try {
    if (res.data) {
      const user = UserService.update(req.params.id, res.data);
      res.data = user;
    }
  } catch (err) {
    res.err = err.message;
  } finally {
    next();
  }
}, responseMiddleware);

router.delete('/:id', function (req, res, next) {
  try {
    const user = UserService.delete(req.params.id)
    res.data = user;
  } catch (err) {
    res.err = err.message;
  } finally {
    next();
  }
}, responseMiddleware);

module.exports = router;