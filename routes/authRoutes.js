const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
    try {
        if (Object.keys(req.body).length !== 0) {
            const data = AuthService.login(req.body);
            res.data = data;
        } else {
            res.err = 'all fields must be filled';
        }
    } catch (err) {
        res.err = err.message;
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;