const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
  // TODO: Implement methods to work with fighters

  create(data) {
    const item = FighterRepository.create(data);
    if (!item) {
      throw Error('Fighter not created!');
    }
    return item;
  }

  getAll() {
    const items = FighterRepository.getAll();
    if (!items) return null;
    return items;
  }

  update(id, dataToUpdate) {
    const item = FighterRepository.update(id, dataToUpdate);
    if (!item) {
      throw Error('Fighter not updated!');
    } else {
      return item;
    }
  }

  delete(id) {
    const item = FighterRepository.delete(id);
    if (!item) {
      throw Error('User not deleted!');
    } else {
      return item;
    }
  }


  search(search) {
    const item = FighterRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }
  //
}
module.exports = new FighterService();