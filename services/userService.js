const { UserRepository } = require('../repositories/userRepository');

class UserService {

  // TODO: Implement methods to work with user
  create(data) {
    const item = UserRepository.create(data);
    if (!item) {
      throw Error('User not created!');
    }
    return item;
  }

  getAll() {
    const items = UserRepository.getAll();
    if (!items) return null;
    return items;
  }

  update(id, dataToUpdate) {
    const item = UserRepository.update(id, dataToUpdate);
    if (!item) {
      throw Error('User not updated!');
    } else {
      return item;
    }
  }

  delete(id) {
    const item = UserRepository.delete(id);
    if (!item) {
      throw Error('User not deleted!');
    } else {
      return item;
    }
  }
  //
  search(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new UserService();